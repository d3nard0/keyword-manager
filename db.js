const Sequelize = require ('sequelize');
const Connection = new Sequelize(
    //'keyword_manager',
    //'denardo',
    //'denardo222',
    'sql7295332',
    'sql7295332',
    'FZZH5CCeB1',
    {
        dialect:'mysql',
        //host:"db4free.net",
        host:"sql7.freesqldatabase.com",
        minConnections: 1,
        maxConnections: 100
    }
);

const Categories = Connection.define('categories' , {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    }
})

const Keywords = Connection.define('keywords' , {
    name: {
        type: Sequelize.STRING,
        allowNull: false
    }
})

//Relationships
Categories.hasMany(Keywords);
Keywords.belongsTo(Categories);

const dbConnect = ()=>{
    Connection.sync({force: false}).then(()=>{
    }).catch(err=> {
        console.log('connection error', err)
        dbConnect();
    });
}
process.on('SIGINT', function() {
    console.log('CLOSING CONNECTION.');
    Connection.authenticate()
        .then(() => {
            console.log('Connection has been established successfully.');
            Connection.close();
            console.log('closed')
            process.exit()
            console.log('process closed')
        })
        .catch(err => {
            console.error('Unable to connect to the database:', err);
            process.exit()
        });
});

dbConnect();
module.exports = Connection;
