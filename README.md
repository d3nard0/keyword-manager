## Keyword Manager

To run the application locally, you have to start both server and frontend application:

     1. Navigate to the root of the folder in the terminal and install node modules: `npm install`
     2. Navigate to the `client` folder in the terminal and install node modules: `npm install`
     3. Navigate back to root folder and start server and client: `npm run dev`

Server will connect to test mysql database

Open [http://localhost:3000](http://localhost:3000) to view it in the browser.

The page will reload if you make edits.<br>
Theme used in this project: https://bootswatch.com/solar/


