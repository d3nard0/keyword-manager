const schema =require('./schema.js');
const express =require('express');
const GraphHTTP =require('express-graphql');
const cors = require('cors');

const APP_PORT = 4000;

const app = express();

app.use(cors());

app.use('/graphql', GraphHTTP({
    schema,
    graphiql: true,
    pretty: true
}))

app.listen(APP_PORT, ()=>{
    console.log(`App listening on port ${APP_PORT}`);
});

