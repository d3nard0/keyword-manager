import React from 'react';
import './style/App.css';
import ApolloClient from 'apollo-boost';
import { ApolloProvider } from 'react-apollo';
import Categories from './components/categories';

const client = new ApolloClient({
  uri:'http://localhost:4000/graphql'
})

function App() {
  return (
    <ApolloProvider client={client}>
      <div className="container">
        <h2>Keyword Manager</h2>
        <Categories />
      </div>
    </ApolloProvider>

  );
}

export default App;
