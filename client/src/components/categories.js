import React, { Component, Fragment } from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import {Query}  from 'react-apollo';
import CategoryItem from './categoryItem';
import addIcon from "../img/confirm.svg";
import cancelIcon from "../img/close.svg";
import refreshIcon from "../img/refresh.svg";

const CATEGORIES_QUERY = gql`
    query CategoriesQuery {
        categories {
            name
            id
            keywords {
                name
                id
            }
        }
    }
`
const ADD_CATEGORY = gql`
    mutation addCategory($name: String!) {
        addCategory(name: $name) {
            id
            name
        }
    }
`
export class Categories extends Component {
    constructor(props, {mutate}){
        super(props);
        this.state = {
            add:false,
            cName:'',
            invalid:false
        }
        this.refetch=null;
        this.addCategory = this.addCategory.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleAddCategory = this.handleAddCategory.bind(this);
        this.handleCancelAddCategory = this.handleCancelAddCategory.bind(this);
        this.handleEnterPress = this.handleEnterPress.bind(this);
    }

    addCategory(){
        this.setState({add:true})
    }

    handleAddCategory(){
        const that = this;
        if(!this.state.cName.length){
            this.setState({invalid:true})
            return;
        }
        this.props.mutate({
            variables: { name: this.state.cName },
            refetchQueries: [ { query: CATEGORIES_QUERY }]
        })
        .then( res => {
            that.handleCancelAddCategory();
        });

    }

    handleChange(evt){
        this.setState({
            cName:evt.target.value,
            invalid:false
        })
    }

    handleCancelAddCategory(){
        this.setState({
            cName:'',
            add: false,
            invalid:false
        })
    }

    handleEnterPress(evt){
        if(evt.keyCode === 13) {
            this.handleAddCategory();
        }
    }

    render() {
        return (
            <Fragment>
                <h3 className="d-flex">
                    <div className="p-2">Categories</div>
                    <div className="p-2">
                        <button className="btn btn-success ml-3" title="Add Category" onClick={this.addCategory}>+</button>
                    </div>
                    <div className="ml-auto p-2">
                        <button className="btn btn-success ml-3" title="Refresh" onClick={()=>{if(this.refetch)this.refetch()}}>
                            <img src={refreshIcon} className="refresh-icon big" alt="refresh" />
                        </button>
                    </div>
                </h3>
                {
                    this.state.add ?
                    <div className="row m-0 mb-2" style={{"height":"60px"}} >
                        <div className="form-group col-sm-4 mb-0 p-0">
                            <input value={this.state.cName} onChange={this.handleChange} onKeyUp={(e)=>this.handleEnterPress(e)} name="name" type="text" className={`form-control ${this.state.invalid ? 'is-invalid':''}`} placeholder="Category name" />
                            {this.state.invalid ?
                                <div className="invalid-feedback">Name is required field</div>
                                :
                                null
                            }
                        </div>
                        <div className="col-sm-2 mt-1" >
                            <img src={addIcon} className="add-icon big" alt="Add Category" onClick={this.handleAddCategory} />
                            <img src={cancelIcon} className="add-icon big ml-1" alt="Cancel" onClick={this.handleCancelAddCategory} />
                        </div>
                    </div>
                    :
                    null
                }
                <Query query={CATEGORIES_QUERY}>
                    {
                        ({loading, error, data, refetch})=>{
                            this.refetch = refetch;
                            if(loading) {
                                return (
                                    <div className="justify-content-center d-flex">
                                        <div className="spinner-border text-warning" role="status">
                                            <span className="sr-only">Loading...</span>
                                        </div>
                                    </div>
                                )
                            }
                            if(error) {
                                console.log('error ', error)
                                return (
                                    <div>Error fetching categories data</div>
                                )
                            }

                            return <Fragment>
                                {
                                    data.categories.map(( category, index)=>(
                                        <CategoryItem key={index} data={category} refetch={refetch}/>
                                    ))
                                }
                            </Fragment>
                        }
                    }
                </Query>
            </Fragment>
        );
    }
}

const CategoriesWithMutation = graphql(ADD_CATEGORY)(Categories);
export default CategoriesWithMutation;
