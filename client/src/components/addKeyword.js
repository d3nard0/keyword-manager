import React, { Component, Fragment } from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import addIcon from '../img/confirm.svg'
import cancelIcon from '../img/close.svg'

const ADD_KEYWORD = gql`
    mutation addKeyword($name: String!, $categoryId: Int!) {
        addKeyword(name: $name, categoryId: $categoryId) {
            id
            name
        }
    }
`

export class AddKeyword extends Component {
    constructor(props){
        super(props);
        this.state = {
            kName:'',
            invalid: false
        };
        this.addKeyword = this.addKeyword.bind(this);
        this.handleChange = this.handleChange.bind(this);
        this.handleAddKeyword = this.handleAddKeyword.bind(this);
        this.handleCancelAddKeyword = this.handleCancelAddKeyword.bind(this);
        this.handleEnterPress = this.handleEnterPress.bind(this);
    }
    handleAddKeyword(){
        const that = this;
        if(!this.state.kName.length){
            this.setState({invalid:true})
            return;
        }
        this.props.mutate({
            variables: { name: this.state.kName, categoryId:this.props.categoryId },
        })
        .then( res => {
            that.props.refetch();
            that.handleCancelAddKeyword();
        });

    }

    handleCancelAddKeyword(){
        this.setState({
            kName:'',
            add: false,
            invalid:false,
        })
    }
    handleChange(evt){
        this.setState({
            kName:evt.target.value,
            invalid:false,
        })
    }
    addKeyword(evt){
        this.setState({
            add:true
        })
    }
    handleEnterPress(evt){
        if(evt.keyCode === 13) {
            this.handleAddKeyword();
        }
    }
    render() {
        return (
            <Fragment>
                {
                    this.state.add ?
                        <span className="badge badge-default ml-3 p-0 pl-2">
                            <div className="row">
                                 <div className="form-group col-sm-10 mb-0 p-0">
                                    <input value={this.state.kName} onChange={this.handleChange} onKeyUp={(e)=>this.handleEnterPress(e)} name="name" type="text" className={`form-control ${this.state.invalid ? 'is-invalid':''}`} placeholder="Keyword name" />
                                     {this.state.invalid ?
                                         <div className="invalid-feedback">Name is required field</div>
                                         :
                                         null
                                     }
                                 </div>
                                <div className="col-sm-2 my-auto">
                                    <img src={addIcon} className="add-icon" alt="Add Keyword" onClick={this.handleAddKeyword} />
                                    <img src={cancelIcon} className="add-icon ml-1" alt="Cancel" onClick={this.handleCancelAddKeyword} />
                                </div>

                            </div>
                        </span>
                    :
                    <button title="Add keyword" className="btn btn-sm btn-success ml-3 mb-1" onClick={this.addKeyword}>+</button>
                }
            </Fragment>

        );
    }

}
const AddKeywordWithMutation = graphql(ADD_KEYWORD)(AddKeyword);
export default AddKeywordWithMutation;
