import React, { Component } from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import KeywordItem from './keywordItem';
import AddKeyword from './addKeyword';
import trashIcon from '../img/trash2.svg';

const DELETE_CATEGORY = gql`
    mutation deleteCategory($id: Int!) {
        deleteCategory(id: $id) {
            id
            name
        }
    }
`

export class CategoryItem extends Component {
    constructor(props){
        super(props);
        this.state = {};
        this.deleteCategory = this.deleteCategory.bind(this)
    }
    deleteCategory(){
        const that = this;
        this.props.mutate({
            variables: { id: this.props.data.id },
        })
        .then( res => {
            that.props.refetch();
        });
    }
    render() {
        const {data} = this.props;
        return (
            <div className="card card-body align-self-center">

                <div className="row">
                    <div className="col-sm-1 my-auto">
                        <img src={trashIcon} alt="Delete category" className="delete-icon" onClick={this.deleteCategory}/>
                    </div>
                    <div className="col-md-3">
                        <div className="text-left" style={{"lineHeight":"61px"}}>{data.name}</div></div>
                    <div className="col-md-8">
                        <div>Keywords</div>
                        {
                            data.keywords.map((key)=>{
                                return(
                                    <KeywordItem key={key.id} data={key} refetch={this.props.refetch}/>
                                )
                            })
                        }
                       <AddKeyword refetch={this.props.refetch} categoryId={data.id}/>
                    </div>
                </div>
            </div>
        );
    }

}
const CategoryItemWithMutation = graphql(DELETE_CATEGORY)(CategoryItem);
export default CategoryItemWithMutation;
