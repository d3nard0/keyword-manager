import React, { Component } from 'react';
import gql from 'graphql-tag';
import { graphql } from 'react-apollo';
import trashIcon from '../img/trash2.svg';

const DELETE_KEYWORD = gql`
    mutation deleteKeyword($id: Int!) {
        deleteKeyword(id: $id) {
            id
            name
        }
    }
`

export class KeywordItem extends Component {
    constructor(props){
        super(props);
        this.state = {};
        this.deleteKeyword = this.deleteKeyword.bind(this)
    }

    deleteKeyword(){
        const that = this;
        this.props.mutate({
            variables: { id: this.props.data.id },
        })
        .then( res => {
            that.props.refetch();
        });
    }
    render() {
        const {data} = this.props;
        return (
            <span className="badge badge-dark ml-3 mt-1">
                <span>{data.name}</span>
                <button className="btn btn-sm danger" style={{"color":"red", "verticalAlign":"inherit"}} onClick={this.deleteKeyword}>
                    <img src={trashIcon} alt="Delete" title="Delete keyword" className="delete-icon-small" />
                </button>
            </span>
        )
    }

}
const KeywordItemWithMutation = graphql(DELETE_KEYWORD)(KeywordItem);
export default KeywordItemWithMutation;
