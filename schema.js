const {
    GraphQLObjectType,
    GraphQLInt,
    GraphQLString,
    GraphQLList,
    GraphQLSchema,
    GraphQLNonNull
} = require( 'graphql')
const Db = require('./db.js');

const Categories = new GraphQLObjectType({
    name: 'Categories',
    description: 'this represents a category',
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                allowNull: false,
                resolve(categories) {
                    return categories.id;
                }
            },
            name: {
                type: GraphQLString,
                allowNull: false,
                resolve(categories) {
                    return categories.name;
                }
            },
            keywords: {
                type: new GraphQLList(Keywords),
                resolve(categories) {
                    return categories.getKeywords();
                }
            }
        }
    }
})

const Keywords = new GraphQLObjectType({
    name: 'Keyword',
    description: 'this is a keyword',
    fields: () => {
        return {
            id: {
                type: GraphQLInt,
                allowNull: false,
                resolve(keywords) {
                    return keywords.id;
                }
            },
            name: {
                type: GraphQLString,
                allowNull: false,
                resolve(keywords) {
                    return keywords.name;
                }
            },
            categoryId: {
                type: GraphQLInt,
                allowNull: false,
                resolve(keywords) {
                    return keywords.categoryId;
                }
            },
            category: {
                type: Categories,
                resolve(keyword) {
                    return keyword.getCategory();
                }
            }
        }
    }
});

const RootQuery = new GraphQLObjectType({
    name: 'Query',
    fields: () => {
        return {
            categories: {
                type: new GraphQLList(Categories),
                args: {
                    id: {
                        type: GraphQLInt
                    },
                    name: {
                        type: GraphQLString
                    }
                },
                resolve(parentValue, args) {
                    return Db.models.categories.findAll({where: args});
                }
            },
            keywords: {
                type: new GraphQLList(Keywords),
                args: {
                    id: {
                        type: GraphQLInt
                    },
                    categoryId: {
                        type: GraphQLInt
                    },
                    name: {
                        type: GraphQLString
                    }
                },
                resolve(parentValue, args) {
                    return Db.models.keywords.findAll({where: args});
                }
            }
        }
    }
})

const Mutation = new GraphQLObjectType({
    name: 'Mutation',
    descriptions: '...',
    fields() {
        return {
            addCategory: {
                type: Categories,
                args: {
                    name: {
                        type: new GraphQLNonNull(GraphQLString)
                    }
                },
                resolve(_, args)
                {
                    return Db.models.categories.create({
                        name: args.name
                    })
                }
            },
            deleteCategory: {
                type: Categories,
                args: {
                    id: {
                        type: new GraphQLNonNull(GraphQLInt)
                    }
                },
                resolve(_, args)
                {
                    return Db.models.keywords.destroy({
                    where:{
                        categoryId: args.id
                    },}).then(()=>{
                        return Db.models.categories.destroy({
                            where:{
                                id: args.id
                            },
                    })})
                }
            },
            addKeyword: {
                type: Keywords,
                args: {
                    name: {
                        type: new GraphQLNonNull(GraphQLString)
                    },
                    categoryId: {
                        type: new GraphQLNonNull(GraphQLInt)
                    }
                },
                resolve(_, args)
                {
                    return Db.models.keywords.create({
                        name: args.name,
                        categoryId: args.categoryId
                    })
                }
            },
            deleteKeyword: {
                type: Keywords,
                args: {
                    id: {
                        type: new GraphQLNonNull(GraphQLInt)
                    }
                },
                resolve(_, args)
                {
                    return Db.models.keywords.destroy({
                    where:{
                        id: args.id
                    }})
                }
            },
        }
    }
})

const Schema = new GraphQLSchema({
    query: RootQuery,
    mutation: Mutation
})

module.exports = Schema;
